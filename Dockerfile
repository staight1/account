FROM java:8-alpine
RUN mkdir -p /tmp/tar
COPY account-0.0.1-SNAPSHOT.jar /tmp/tar/account-0.0.1-SNAPSHOT.jar
WORKDIR /tmp/tar
RUN cd /tmp/tar
EXPOSE 8506
ENTRYPOINT ["sh","-c","java -Xmx500m -jar account-0.0.1-SNAPSHOT.jar --spring.profiles.active=prod"]